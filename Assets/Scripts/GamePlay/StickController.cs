using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickController : MonoBehaviour
{
    public float speed;

    private float m_curSpeed;
    [SerializeField]private float m_limitOffset;
    private float m_minX;
    private float m_maxX;
    private Vector2 m_startingPos;

    private Rigidbody2D m_rb;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_startingPos = transform.position;
    }

    private void Start()
    {
        GetLimitPos();        
    }

    private void FixedUpdate()
    {
        if (!GameController.ins ||!m_rb) return;

        if(GameController.ins.CanMoveLeft)
        {
            m_curSpeed = speed * -1;
        }
        else if(GameController.ins.CanMoveRight)
        {
            m_curSpeed = speed;
        }
        else
        {
            m_curSpeed = 0;
        }

        m_rb.velocity = Vector2.right * m_curSpeed;

    }

    private void Update()
    {
        float limitPosX = Mathf.Clamp(transform.position.x, m_minX, m_maxX);
        transform.position = new Vector3(limitPosX, m_startingPos.y, 0);
    }

    //private void GetLimitOffSet()
    //{
    //    Collider2D collider = GetComponent<Collider2D>();
    //    if (!collider) return;
    //    m_limitOffset = collider.bounds.size.x; 
    //}

    private void GetLimitPos()
    {
        //GetLimitOffSet();   
        m_minX = ViewPortUtil.MinX + (ViewPortUtil.MaxX - ViewPortUtil.MinX) / 10f * 4f;
        m_maxX = ViewPortUtil.MinX + (ViewPortUtil.MaxX - ViewPortUtil.MinX) / 10f * 10f;
    }
}
