using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Level", fileName = "Level.asset")]
public class DataStruct : ScriptableObject
{
    public int id;
    public int scoreRequire;
    public int boom;
    public int coin;
    public float speedBottle;
    public float bottleSpawnTime;
    public Sprite unlockImage;
}
